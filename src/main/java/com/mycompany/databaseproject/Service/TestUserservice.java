/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.Service;
import com.mycompany.databaseproject.model.User;


/**
 *
 * @author INGK
 */
public class TestUserservice {
    public static void main(String[] args) {
        Userservice userService = new Userservice();
        User user = userService.login("user2", "password1");
        if(user != null){
            System.out.println("Welcome user: " + user.getName());
        } else {
            System.out.println("Error");
        }
    }
}

