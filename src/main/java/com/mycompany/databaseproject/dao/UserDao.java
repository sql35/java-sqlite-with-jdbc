/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.dao;

import com.mycompany.databaseproject.DatabaserHelper.DatabaseHelper;
import com.mycompany.databaseproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author INGK
 */
public class UserDao implements Dao<User>{

    @Override
    public User get(int id) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user WHERE user_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return user;
    }

    @Override
    public List<User> getALL() {
     // selection
        ArrayList<User> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user ";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
        public List<User> getAll(String where, String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM user where " + where + "ORDER BY " + order;
        System.out.println(sql);
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);
                list.add(user);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }    
    
    

    @Override
    public User save(User obj) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        String sql = "INSERT INTO user ( user_name, user_gender , user_password, user_role"
                + "VALUES (?, ?, ?, ?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getName());
            stmt.setString(2,obj.getGender());
            stmt.setString(3,obj.getPassword());
            stmt.setInt(4,obj.getRole());
            System.out.println(stmt);

            obj.setId(DatabaseHelper.getInsertedId(stmt));
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

    @Override
    public User update(User obj) {

        String sql = "UPDATE user\n"
                + " SET user_name =?, user_gender = ?, user_password = ?, user_role = ?"
                + " WHERE user_id =?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1,obj.getName());
            stmt.setString(2,obj.getGender());
            stmt.setString(3,obj.getPassword());
            stmt.setInt(4,obj.getRole());
            stmt.setInt(5,obj.getId());
            
            stmt.executeUpdate();            
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(User obj) {
        User user = null;
        String sql = "DELETE FROM user WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
